/*
	Mini-Activity: 10 minutes, p

	Create a function called displayMsgToSelf()
	-display a message to your past self in your console

	Invoke the function 10 times.

*/

// [Try-Catch-Finally Statement]
/*

	- "try catch" statements are commonly used for error handling
	-there are instances when the application returns an error/warning that is not necessarily an error in the context of our code
	- these errors are a result of an attempt of the progrmaming language to help developer in creating efficient of our code
	- they are used to specify a response whenever an exception/error is received
	- it is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statements
	- In porgramming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code
	- The "finally" block is used to specify a response/action that is used to handle/resolve errors.
*/

function showIntensityAlert(windSpeed) {
	try {
		//Attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));

		//error/err are commonly used variables used by the developers for storing errors
	} catch (error) {
		// "typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is
		console.log(typeof error);

		// catch errors within "try" statement
		// in this case the error is an unknown function "alert" which does not exist in JS
		// the 'alert' function is used similarly to a prompt to alert the user
		// 'error.message' is used to access the information relating to an error object
		console.log(error.message);

	} finally {
		// continue execution of code regardless of success and failure or code execution in the 'try' block to handle/resolve errors.
		alert('Intensity updates will show new alert.');

	}
}

showIntensityAlert(56);

// const num = 100, x = 'a';

// try {
// 	console.log(num/x);
// 	console.log(a);
// } catch (error) {
// 	console.log('An error caught.');
// 	console.log('Error message: ' + error);
// } finally {
// 	alert('Finally will execute.');
// }

// Mini-Activity
/*
	Create a gradeEvaluator function with the try-catch-statement if the grade inputs a letter instead of a number

	using the selection operator
		- Evaluate the grade input and return the letter distinction
			- if the grade is less or equal to 70 = F
			- if the grade is greater than or equal to 71 = C
			- if the grade is greater than or equal to 80 = B
			- if the grade is greater than or equal to 90 = A

		- if the use input the character
			- the function will catch the error
			-alert a message regardless the success or failure of the system
*/



// Do While Loop
	// is similar to while loop. However, this will run the code block at least once before it checks the condition.



let doWhileCounter = 20

do {
	console.log(doWhileCounter)
	--doWhileCounter
} while(doWhileCounter < 0)


// For Loop
	// A for loop is more flexible than while and do-while loops.
	// It consists of 3 parts:
		// 1. The "initialization" value that will track the progression of the loop.
		// 2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time or not.
		// 3. The "finalExpression" indicates how to advance the loop


/*
	Syntax:

	for(initialization, condition, finalExpression){
		
		codeblock / statement
	}
*/

	// Create a loop that will start from 1 and end at 20

	for (let x = 1; x <= 20; x++){
		console.log(x)
	}


// Q: Can we use for loop in a string?
let name = "Nikko Handsome";

// Accessing elements of a string
	// using index
	// index starts at 0
console.log(name[0])
console.log(name [1])
console.log(name[2])

// Count of the string characters
	// using .length
console.log(name.length) //3 characters

// Use for loop to display each character of the string
for(let index = 0; index < name.length; index++){
	console.log(name[index])
}


// Using for loop in an array
let fruits = ["mango", "apple", "orange", "banana", "strawberry", "kiwi"]
	// elements = each value inside the array
console.log(fruits)

// Total count of elements in an array
console.log(fruits.length)  //6 elements

// Access each element in an array
console.log(fruits[0]) //mango
console.log(fruits[4]) //strawberry
console.log(fruits[5]) //kiwi


// How do we determine the last element in an array if we don't know the total number of the elements.
console.log(fruits[fruits.length - 1])
//console.log(fruits[6])

// if we want to assign new value to an element
fruits[6] = "grapes"
console.log(fruits)

// Using for loop in an array
for(let i = 0; i < fruits.length; i++){
	console.log(fruits[i])
}




// Example of Array of Objects
let cars = [
	{
		brand: "Toyota",
		type: "Sedan"
	},
	{
		brand: "Rolls Royce",
		type: "Luxury Sedan"
	},
	{
		brand: "Mazda",
		type: "Hatchback"
	}
];

console.log(cars.length) //3 elements
console.log(cars[1])


// Mini Activity
	// Use for loop to display each element in the cars array

	// SS your code && console output and send it to hangouts


	for(let i = 0; i < cars.length; i++){
	console.log(cars[i])
}



/*

1. Create a string named "myName" with a value of your name

2. Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel
	- How this For Loop works:
	    1. The loop will start at 0 for the the value of "i"
	    2. It will check if "i" is less than the length of myName (e.g. 0)
	    3. The if statement will check if the value of myName[i] converted to a lowercase letter is equivalent to any of the vowels (e.g. myName[0] = a, myName[0] = e, myName[0] = i, myName[0] = o, myName[0] = u)
	    4. If the expression/condition is true the console will print the number 3.
	    5. If the letter is not a vowel the console will print the letter
	    6. The value of "i" will be incremented by 1 (e.g. i = 1)
	    7. Then the loop will repeat steps 2 to 6 until the expression/condition of the loop is false
*/

let myName = "keaNe"
for(let i = 0; i < myName.length; i++){
	
	if(
		myName[i].toLowerCase() == "k" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "N" ||
		myName[i].toLowerCase() == "e" 
	){
		console.log(3)
	} else {
		console.log(myName[i])
	}
}



// Continue and Break
	// The "Continue" statement allows us to go to the next iteration of the loop "without finishing the execution of the following statements in a code block"

	// console.log(100/3)
	// 33.3
	// (100 % 3) //1

	for(let a = 20; a > 0; a--){
		if(a % 2 === 0){
			continue
		}
		console.log(a)

		if(a < 10){
			break
		}
	}


/* Mini Activity
    - Creates a loop that will iterate based on the length of the string
    - How this For Loop works:
        1. The loop will start at 0 for the the value of "i"
        2. It will check if "i" is less than the length of name (e.g. 0)
        3. The if statement will check if the value of name[i] converted to a lowercase letter a (e.g. name[0] = a)
        4. If the expression/condition of the if statement is true the loop will continue to the next iteration.
        5. If the value of name[i] is not equal to a, the second if statement will be evaluated
        6. The second if statement will check if the value of name[i] converted to a lowercase letter d (e.g. name[0] = d)
        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
        8. The value of "i" will be incremented by 1 (e.g. i = 1)
        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] = d) is true, the loop will stop due to the "break" statement
*/


let string = "alexandro"
for(let i = 0; 1 < myName.length; i++){

	if(string[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration")
		continue;
	}

	if(string[i].toLowerCase() == "d"){
		break;
	}
}



// Q: Is it possible to have another loop inside a loop?
	// A: yes. it is called "Nested Loops"


// Nested Loops

	for(let x = 0; x <= 10; x++){
		console.log(x)

		for(let y = 0; y <= 10; y++){
			console.log(`${x} + ${y} = ${x+y}`)
		}
	}

