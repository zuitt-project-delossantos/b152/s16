
/*
	Mini-Activity: 10 minutes, post your answer in our 152 Hangouts

	Create a function called displayMsgToSelf()
		- display a message to your past self in your consolde

	Invoke the function 10 times.
*/

// [Try-Catch-Finally Statement]
/*
	- "try catch" statements are commonly used for error handling
	- there are instances when the application returns an error/warning that is not necessarily an error in the context of our code
	- these errors are a result of an attempt of the programming language to help developer in creating efficient code
	- they are used to specify a response whenever an exception/error is received
	- it is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statements
	- In programming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code
	- The "finally" block is used to specify a response/action that is used to handle/resolve errors.
 */

// function showIntensityAlert(windSpeed) {
// 	try {
// 		// Attempt to execute a code
// 		alert(determineTyphoonIntensity(windSpeed));

// 		// error/err are commonly used variable names used by the developers for storing errors
// 	} catch (error) {
// 		// "typeof" operator is used to check the date type of a value/expression and returns a string value of what the data type is
// 		console.log(typeof error);

// 		// catch errors within "try" statement
// 		// in this case the error is an unknown function "alert" which does not exist in Javascript
// 		// the 'alert' function is used similarily to a prompt to alert the user
// 		// 'error.message' is used to access the information relating to an error object
// 		console.log(error.message);

// 	} finally {
// 		// continue execution of code regardless of success and failure or code execution in the 'try' block to handle/resolve errors.
// 		alert('Intensity updates will show new alert.');

// 	}
// }

// showIntensityAlert(56);

// const num = 100, x = 'a';

// console.log(num);
// try {
// 	console.log(num/x);
// 	console.log(a);
// } catch (error) {
// 	console.log('An error caught.');
// 	console.log('Error message: ' + error);
// } finally {
// 	alert('Finally will execute.')
// }

// Mini-Activity

/* 
	Create a gradeEvaluator function with the try-catch-finally statement if the user inputs a char/letter instead of a number
		using the selection operator 
			- Evaluate the grade input and return the letter distinction
				- if the grade is less or equal to 70 = F
				- if the grade is greater than or equal to 71 = C
				- if the grade is greater than or equal to 80 = B
				- if the grade is greater than or equal to 90 = A
			- if the user input the character
				- the function will catch the error
				- it prompt the error message
				- alert a message regardless the succes or failure of the system.
			- Post your answer in our B152 Hangouts, both the code and sample output

	Quick break : 10:21 - 10:40
	Mini-Activity: 10:40 - 11:10 Am
	Resume at 11:10
 */

// Solution
// function gradeEvaluator(grade) {
// 	try {
// 		if(grade >= 90) {
// 			return "A"
// 		} else if (grade >= 80) {
// 			return "B"
// 		} else if (grade >= 71) {
// 			return "C"
// 		} else if (grade >= 70) {
// 			return "F"
// 		} else if(typeof grade !== "number") {
// 			throw Error("Invalid input");
// 		}
// 	} catch (error) {
// 		console.log(error);
// 	} finally {
// 		alert("Let's continue!");
// 	}
// }

// gradeEvaluator("8");

function displayMsgToSelf(){
	console.log("Don't text him back.")
}

displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();

let count = 10;

while(count !== 0) {
	displayMsgToSelf();
	count--;
}

// While Loop
// 	allows us to repeat an action or an instruction as long as the condition is true.
/*

	1st loop - count 10
	2nd loop - count 9
	3rd loop - count 8
	4th loop - count 7
	5th loop - count 6
	6th loop - count 5
	7th loop - count 4
	8th loop - count 3
	9th loop - count 2
	10th loop - count 1

	while loop checked if count is still NOT equal to 0:
	at this point, before a possible 11th loop, count is decremented to 0
	therefore, there was no 11th loop

	If there is no decrementration, the condition is always true, thus, a infinite loop.

	Infinite loops will run your code block forever until you stop it.

	an infinite loop is a piece of code that keeps running because a terminating condition is never reached. This may cause your app, browser or even PC to crash.

	Samples:
		while(true) {
			console.log('This will result an infinite loop.')
		}

	Always make sure that at the very least your loop condition will be terminated or will be false at one point.
 */

/* Mini-Activity: 5 minutes

	create a while loop that should display the number from 1-15 (in order):

*/


//Do While Loop
	//Do While loop is similar to While loop. However, Do While loop will run the code block at least once before it checks the condition

	//With While loop, we first check the condition and then run the code block/statements
let doWhileCoutner = 20

do {
	console.log(doWhileCoutner)
	--doWhileCoutner

} while (doWhileCoutner < 0)


// For loop
	// A for loop is more flexible than while and do-while loops. 
	// It consists of 3 parts:
		// 1. The "initialization" value that will track the progression of the loop
		// 2. The "expression/ condition" that will be evaluated which will determine whether the loop will run one more time.
		// 3. The "finexExpression" indicated how to advance the loop
/*
	Syntax:

	for(initialization, condition, finalExpression){
		
		//codeblock / statement
	}
*/

	//Create a loop that will start from 1 and end at 20
	for(let x = 1; x <= 20; x++){
		console.log(x)
	}

//Q: Can we use for loop in a string? - Yes
let name = "Nikko Handsome";

//Accessing elements of a string
	// using index
	// index starts at 0
console.log(name[0])	//N
console.log(name[1])	//i
console.log(name[2])	//k

//Count of the string characters
	//using .length
console.log(name.length)	//3 characters

//Use for loop to display each character of the string
for(let index = 0; index < name.length; index++){
	console.log(name[index])
}


//Using for loop in an array
let fruits = ["mango", "apple", "orange", "banana", "strawberry", "Kiwi"]
	//elements - each value inside the array
console.log(fruits)

//Total count of elements in an array
console.log(fruits.length)	//6 elements

//Access each element in an array
console.log(fruits[0])	//mango
console.log(fruits[4])	//strawberry
console.log(fruits[5])	//Kiwi

//How do we determine the last element in an array if we don't the total number of the elements
console.log(fruits[fruits.length - 1])
// console.log(fruits[6])

//if we want to assign new value to an element
	//assignment operator
fruits[6] = "grapes"
console.log(fruits)

//Using for loop in an array
for(let i = 0; i < fruits.length; i++){
	console.log(fruits[i])
}


//Example of Array of objects
let cars = [
	{
		brand: "Toyota",
		type: "Sedan"
	},
	{
		brand: "Rolls Royce",
		type: "Luxury Sedan"
	},
	{
		brand: "Mazda",
		type: "Hatchback"
	}
];

console.log(cars.length)	//3 elements

console.log(cars[1])

//Mini Activity
	// Use for loop to display each element in the cars array

	//Screenshot your code && console output and send it to our group hangouts

let y = 0;

for(y; y < cars.length; y++){
	console.log(cars[y])
}

let myName = "adrIAn madArang"

/*Mini Activity

2. Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel
	- How this For Loop works:
	    1. The loop will start at 0 for the the value of "i"
	    2. It will check if "i" is less than the length of myName (e.g. 0)
	    3. The if statement will check if the value of myName[i] converted to a lowercase letter is equivalent to any of the vowels (e.g. myName[0] = a, myName[0] = e, myName[0] = i, myName[0] = o, myName[0] = u)
	    4. If the expression/condition is true the console will print the number 3.
	    5. If the letter is not a vowel the console will print the letter
	    6. The value of "i" will be incremented by 1 (e.g. i = 1)
	    7. Then the loop will repeat steps 2 to 6 until the expression/condition of the loop is false
*/

for(let i = 0; i < myName.length; i++){

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
	){
		console.log(3)

	} else {
		console.log(myName[i])
	}
}


// Continue and Break
	// The "Continue" statement allwos the code to go to the next iteration of the loop "without finishing the execution of the following statements in a code block"
		//- skips the current loop and proceed to the next loop


	//console.log(100 / 3)	//33.33
	//console.log(100 % 3)	//1

	for(let a = 20; a > 0; a--){

		if(a % 2 === 0){
			continue
		}

		
		if(a < 10){
			break
		}

		console.log(a)
	}

/* Mini Activity
    - Creates a loop that will iterate based on the length of the string
    - How this For Loop works:
        1. The loop will start at 0 for the the value of "i"
        2. It will check if "i" is less than the length of name (e.g. 0)
        3. The if statement will check if the value of name[i] converted to a lowercase letter a (e.g. name[0] = a)
        4. If the expression/condition of the if statement is true the loop will continue to the next iteration.
        5. If the value of name[i] is not equal to a, the second if statement will be evaluated
        6. The second if statement will check if the value of name[i] converted to a lowercase letter d (e.g. name[0] = d)
        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
        8. The value of "i" will be incremented by 1 (e.g. i = 1)
        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] = d) is true, the loop will stop due to the "break" statement
*/
let string = "alexandro"
for(let i = 0; i < string.length; i++){
	

	if(string[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration")
		continue;
	}


	if(string[i].toLowerCase() == "d"){
		break;
	}
}


//Q: Is it possible to have another loop inside a loop? -Yes

//Nested Loops

	for(let x = 0; x <= 10; x++){
		console.log(x)

		for(let y = 0; y <= 10; y++){
			console.log(`${x} + ${y} = ${x+y}`)
		}
	}

